package com.battcn.system.facade;


import com.battcn.framework.mybatis.service.BaseService;
import com.battcn.system.pojo.po.ProxyPool;

/**
 * @author Levin
 */
public interface ProxyPoolService extends BaseService<ProxyPool> {


}
