package com.battcn.demo;

/**
 * @author Levin
 * @since 2018/03/08
 */
public interface DemoService {

    /**
     * 测试使用
     *
     * @param name 联系人
     * @return 回复信息
     */
    String sayHello(String name);

}
